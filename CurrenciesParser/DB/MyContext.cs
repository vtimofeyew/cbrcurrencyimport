﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class MyContext : DbContext
    {

        public void Add(Models.Currency entity)
        {

        }

        public void Add(Models.Course entity)
        {

        }

        public DbSet<Models.Currency> Currencies {get;set;}
        public DbSet<Models.Course> Courses { get; set; }

    }

}
