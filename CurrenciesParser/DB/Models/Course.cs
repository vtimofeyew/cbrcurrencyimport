﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Course
    {
        public int id { get; set; }
        public DateTime course_date { get; set; }
        public int currency_id { get; set; }
        public decimal course_value { get; set; }
    }
}
