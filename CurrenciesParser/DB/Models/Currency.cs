﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Currency
    {
        
        public int Id { get; set; }
        public string Cbr_id { get; set; }
        public string Name { get; set; }
        public string Eng_Name { get; set; }
        public string Nominal { get; set; }
        public string Parent_Code { get; set; }
        public string ISO_Num_Code { get; set; }
        public string ISO_Char_Code { get; set; }

    }
}
