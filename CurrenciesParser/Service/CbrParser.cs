﻿using CurrenciesParser.Service.XmlModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using RestSharp;

namespace CurrenciesParser.Service
{
    public static class CbrParser
    {
        public const string uriDaily = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=";
        public const string uriAllCurrencies = "http://www.cbr.ru/scripts/XML_valFull.asp";

        public static List<Item> GetAllCurrencies()
        {
            var client = new RestClient(uriAllCurrencies);
            var request = new RestRequest();
            var response = client.Execute<List<Item>>(request).Data;

            return response;


        }

        public static List<Valute> GetCoursesOnDate(DateTime date)
        {

            var client = new RestClient($"{uriDaily}{date:d}");
            var request = new RestRequest();
            var response = client.Execute<List<Valute>>(request).Data;

            // TODO порешать с кодировкой ответа, найти вариант установки кодировки для rest sharp


            return response;

        }
    }
}
