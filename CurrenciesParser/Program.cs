﻿using CurrenciesParser.Service;
using Microsoft.EntityFrameworkCore;
using System;
using System.Text;
using Data;

namespace CurrenciesParser
{
    class Program
    {
        static void Main(string[] args)
        {

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var allCurrencies = CbrParser.GetAllCurrencies();


            using (MyContext ctx = new MyContext())
            {
                foreach (var rec in allCurrencies)
                {
                    //ctx.Add(rec);   // TODO реализовать конвертер моделей и в случае наличия в бд метод update
                }


                // обработка курсов на сегодня
                var actualCurrencies = CbrParser.GetCoursesOnDate(DateTime.Today);
                foreach (var rec in actualCurrencies)
                {
                    //ctx.Add(rec);   // TODO реализовать конвертер моделей и в случае наличия в бд метод update
                }
            }


            



        }
    }
}
